#include <thread>
#include <mutex>

#include <cstdint>
#include <iostream>
#include <utility>
#include <ctime>
#include <string>
#include <memory>

#define TOBACO 1
#define PAPER 2
#define MATCH 3

/*
use g++ to compile this code
~$ g++ cigarette_smokers.cpp -lpthread  -std=c++11
*/

using namespace std;

mutex smoke, check;

bool shouldStop = false;

struct smoker
{
    bool hasTobacco;
    bool hasPaper;
    bool hasMatch;

    smoker(int t, int p, int m):
        hasTobacco(t),
        hasPaper(p),
        hasMatch(m) {}
};

void smokeCigarrete(shared_ptr<smoker> s);

void createCigarreteTobacco(
    shared_ptr<smoker> s,
    shared_ptr<int> prod1,
    shared_ptr<int> prod2,
    shared_ptr<bool> change
)
{
    while(!shouldStop)
    {
        {
            {
                lock_guard<mutex> lock(check);
                if((*prod1 == MATCH || *prod1 == PAPER) 
                    && 
                    ((*prod2 == MATCH || *prod2 == PAPER)))
                {
                    lock_guard<mutex> lock(smoke);
                    smokeCigarrete(s);
                }
            }
            *change = true;
            this_thread::sleep_for(chrono::milliseconds(50));
        }
    }
}

pair<int, int> generate_two_products()
{
    srand(time(0));

    int num = rand() % 3 + 1;

    switch (num)
    {
        case TOBACO:
            return make_pair(PAPER, MATCH);
        
        case PAPER:
            return make_pair(TOBACO, MATCH);

        case MATCH:
            return make_pair(PAPER, TOBACO);
    }
}

void smokeCigarrete(shared_ptr<smoker> s)
{
    string str = "";
    if(s -> hasTobacco)    str += "smoker having tobaco";
    if(s -> hasPaper)    str += "smoker having paper";
    if(s -> hasMatch)    str += "smoker having match";
    cout << str << " is smoking.." << endl;
    this_thread::sleep_for(chrono::milliseconds(100));
}

void createCigarretePaper(
    shared_ptr<smoker> s,
    shared_ptr<int> prod1,
    shared_ptr<int> prod2,
    shared_ptr<bool> change
)
{
    while(!shouldStop)
    {
        {
            {
                lock_guard<mutex> lock(check);
                if((*prod1 == MATCH || *prod1 == TOBACO) 
                    && 
                    ((*prod2 == MATCH || *prod2 == TOBACO)))
                {
                    lock_guard<mutex> lock(smoke);
                    smokeCigarrete(s);
                }
            }    
            *change = true;
            this_thread::sleep_for(chrono::milliseconds(50));
        }
    }
}

void createCigarreteMatch(
    shared_ptr<smoker> s,
    shared_ptr<int> prod1,
    shared_ptr<int> prod2,
    shared_ptr<bool> change
)
{
    while(!shouldStop)
    {
        {
            {
                lock_guard<mutex> lock(check);
                if((*prod1 == PAPER || *prod1 == TOBACO) 
                    && 
                    ((*prod2 == PAPER || *prod2 == TOBACO)))
                {
                    lock_guard<mutex> lock(smoke);
                    smokeCigarrete(s);
                }
            }
            *change = true;
            this_thread::sleep_for(chrono::milliseconds(50));
        }
    }
}

void stopThreads()
{
    cin.get();
    shouldStop = true; 
}

void simulate(shared_ptr<smoker> s1,
              shared_ptr<smoker> s2,
              shared_ptr<smoker> s3)
{
    pair<int, int> p = generate_two_products();

    shared_ptr<int> prod1 = make_shared<int>();
    shared_ptr<int> prod2 = make_shared<int>();
    shared_ptr<bool> change = make_shared<bool>();

    *prod1 = get<0>(p);
    *prod2 = get<1>(p);

    thread t1(&createCigarreteTobacco, s1, prod1, prod2, change);
    thread t2(&createCigarretePaper, s2, prod1, prod2, change);
    thread t3(&createCigarreteMatch, s3, prod1, prod2, change);

    thread stop(&stopThreads);

    while(!shouldStop)
    {
        if(*change)
        {
            lock_guard<mutex> lock(check);
            p = generate_two_products();

            *prod1 = get<0>(p);
            *prod2 = get<1>(p);
        }
    }

    t1.join();
    t2.join();
    t3.join();
    stop.join();
}

int main(int argc, char* argv[])
{
    shared_ptr<smoker> smoker1 = make_shared<smoker>(1, 0, 0);
    shared_ptr<smoker> smoker2 = make_shared<smoker>(0, 1, 0);
    shared_ptr<smoker> smoker3 = make_shared<smoker>(0, 0, 1);

    simulate(smoker1, smoker2, smoker3);
}
  

/*	***output***

smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having tobaco is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having paper is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
smoker having match is smoking..
.
.
.
.
.
.
.

*/