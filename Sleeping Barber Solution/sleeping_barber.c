#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <errno.h>
#include <sys/ipc.h>
#include <semaphore.h>

#define N 5

/*
use gcc to compile this code
~$ gcc sleeping_barber.c -lpthread
*/

time_t replace_time;

sem_t mutex,customers,barbers;
int countNum=0;

void *barber(void *arg)/*Barber Process*/
{
    while(time(NULL)<replace_time || countNum>0)
    {
        sem_wait(&customers);            
        sem_wait(&mutex);
        countNum--;
        printf("Barber:cutting hair,countNum is:%d.\n",countNum);
        sem_post(&mutex);
        sem_post(&barbers);
        sleep(3);       
    }
}


void *customer(void *arg)/*Customers Process*/
{
    while(time(NULL)<replace_time)
    {
        sem_wait(&mutex);
        if(countNum<N)
        {
            countNum++;
            printf("Customer:adding countNum,countNum is:%d\n",countNum);
            sem_post(&mutex);
            sem_post(&customers);
            sem_wait(&barbers);
        }
        else
            sem_post(&mutex);
        sleep(1);
    }
}

int main(int argc,char *argv[])
{
    pthread_t id1,id2;
    int status=0;
    replace_time=time(NULL)+10;

    sem_init(&mutex,0,1);
    sem_init(&customers,0,0);
    sem_init(&barbers,0,1);

    status=pthread_create(&id1,NULL,barber,NULL);
    if(status!=0)
        perror("create barber is failure!\n");
    status=pthread_create(&id2,NULL,customer,NULL);
    if(status!=0)
        perror("create customer is failure!\n");

    pthread_join(id2,NULL);
    pthread_join(id1,NULL);

    exit(0);
}
/*  ***Output***

Customer:add count,count is:1
Barber:cut hair,count is:0.
Customer:add count,count is:1
Customer:add count,count is:2
Barber:cut hair,count is:1.
Customer:add count,count is:2
Barber:cut hair,count is:1.
Customer:add count,count is:2
Barber:cut hair,count is:1.
Barber:cut hair,count is:0.

*/