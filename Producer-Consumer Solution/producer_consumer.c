#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

/*
	use gcc to compile this code
	~$ gcc producer_consumer.c -lpthread
*/

pthread_t *producers;
pthread_t *consumers;

sem_t mutex,empty,fill;

int *buf,buf_pos=-1,numberOfProducers,numberOfConsumers,bufferLength;

int produce(pthread_t self){
	int i = 0;
	int item_produced = 1 + rand()%40;
	while(!pthread_equal(*(producers+i),self) && i < numberOfProducers){
		i++;
	}
	printf("Producer %d produce: %d \n",i+1,item_produced);
	return item_produced;
}


void consume(int p,pthread_t self){
	int i = 0;
	while(!pthread_equal(*(consumers+i),self) && i < numberOfConsumers){
		i++;
	}

	printf("Buffer:");
	for(i=0;i<=buf_pos;++i)
		printf("%d ",*(buf+i));
	printf("\nConsumer %d consume: %d \nCurrent buffer length is: %d\n",i+1,p,buf_pos);
	
}


void* producer(void *args){

	while(1){
		int item_produced = produce(pthread_self());
		sem_wait(&empty); 
		sem_wait(&mutex);
		++buf_pos;
		*(buf + buf_pos) = item_produced; 
		sem_post(&mutex);
		sem_post(&fill);
		sleep(1 + rand()%3);
	}
	
	return NULL;
}


void* consumer(void *args){
	int c;
	while(1){
		sem_wait(&fill);
		sem_wait(&mutex);
		c = *(buf+buf_pos);
		consume(c,pthread_self());
		--buf_pos;
		sem_post(&mutex);
		sem_post(&empty);
		sleep(1+rand()%5);
	}

	return NULL;
}

int main(void){
	
	srand(time(NULL));

	sem_init(&mutex,0,1);
	sem_init(&fill,0,0);

	printf("Enter the no. of Producers:");
	scanf("%d",&numberOfProducers);
	producers = (pthread_t*) malloc(numberOfProducers*sizeof(pthread_t));

	printf("Enter the no. of Consumers:");
	scanf("%d",&numberOfConsumers);
	consumers = (pthread_t*) malloc(numberOfConsumers*sizeof(pthread_t));

	printf("Enter buffer capacity:");
	scanf("%d",&bufferLength);
	buf = (int*) malloc(bufferLength*sizeof(int));

	sem_init(&empty,0,bufferLength);

	for(int i=0;i<numberOfProducers;i++){
		int err = pthread_create(producers+i,NULL,&producer,NULL);
		if(err != 0){
			printf("Error create producer %d: %s\n",i+1,strerror(err));
		}
	}

	for(int i=0;i<numberOfConsumers;i++){
		int err = pthread_create(consumers+i,NULL,&consumer,NULL);
		if(err != 0){
			printf("Error create consumer %d: %s\n",i+1,strerror(err));
		}
	}
	for(int i=0;i<numberOfProducers;i++){
		pthread_join(*(producers+i),NULL);
	}
	for(int i=0;i<numberOfConsumers;i++){
		pthread_join(*(consumers+i),NULL);
	}


	return 0;
}

/*	***Output***

Enter the no. of Producers:2
Enter the no. of Consumers:3
Enter buffer capacity:5
Producer 1 produce: 21
Buffer:21
Consumer 2 consume: 21
Current buffer length is: 0
Producer 2 produce: 3
Buffer:3
Consumer 2 consume: 3
Current buffer length is: 0
Producer 2 produce: 16
Buffer:16
Consumer 2 consume: 16
Current buffer length is: 0
Producer 1 produce: 14
Buffer:14
Consumer 2 consume: 14
Current buffer length is: 0
Producer 2 produce: 36
Producer 1 produce: 15
Buffer:36 15
Consumer 3 consume: 15
Current buffer length is: 1
Producer 2 produce: 40
Buffer:36 40
Consumer 3 consume: 40
Current buffer length is: 1
Buffer:36
Consumer 2 consume: 36
Current buffer length is: 0
Producer 1 produce: 23
Producer 2 produce: 1
Producer 1 produce: 24
Buffer:23 1
Consumer 3 consume: 1
Current buffer length is: 1
Producer 2 produce: 11
Producer 1 produce: 20
Producer 2 produce: 23
Buffer:23 24 11 20 23
Consumer 6 consume: 23
Current buffer length is: 4
Buffer:23 24 11 20
Consumer 5 consume: 20
Current buffer length is: 3
Producer 1 produce: 33
Buffer:23 24 11 33
Consumer 5 consume: 33
Current buffer length is: 3
Producer 2 produce: 6
Buffer:23 24 11 6
Consumer 5 consume: 6
Current buffer length is: 3
Buffer:23 24 11
Consumer 4 consume: 11
Current buffer length is: 2
Producer 2 produce: 2
Producer 1 produce: 12
Buffer:23 24 2 12
Consumer 5 consume: 12
Current buffer length is: 3
Producer 2 produce: 1
Buffer:23 24 2 1
Consumer 5 consume: 1
Current buffer length is: 3
Producer 1 produce: 21
.
.
.
.
.
.
.

*/